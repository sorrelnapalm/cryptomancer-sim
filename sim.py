#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import random
import sqlite3

class Error(Exception):
    pass

class StatInvalidError(Error):
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message

class AttributeInvalidError(StatInvalidError):
    pass

class CoreRankInvalidError(StatInvalidError):
    pass

class Stats:
    def _assign_stat_lists(self):
        # assign stat tuples for easier indexing?
        self.core = [self.spd, self.pwr, self.wits, self.rslv]
        self.attribs = [self.agi, self.dex, self.stren, self.end,
                        self.know, self.cunning, self.pres, self.will]

    def _set_stats(self, spd=6, pwr=6, wits=6, rslv=6,
        agi=3, dex=3, stren=3, end=3, know=3, cunning=3, pres=3,
        will=3):

        self.spd = spd
        self.pwr = pwr
        self.wits = wits
        self.rslv = rslv

        # error checking

        # is each core rank either 4, 6, or 8?
        message = None
        if spd not in (4, 6, 8):
            message = "Speed must be either 4, 6, or 8!"
        if pwr not in (4, 6, 8):
            message = "Power must be either 4, 6, or 8!"
        if wits not in (4, 6, 8):
            message = "Wits must be either 4, 6, or 8!"
        if rslv not in (4, 6, 8):
            message = "Resolve must be either 4, 6, or 8!"

        if message is not None:
            raise CoreRankInvalidError(None, message)

        # is each attribute >=1 and <=5?
        message = None
        if agi not in range(1, 6):
            message = "Agi must be in the range of 1 to 5 inclusive!"
        if dex not in range(1, 6):
            message = "Dex must be in the range of 1 to 5 inclusive!"
        if stren not in range(1, 6):
            message = "Strength must be in the range of 1 to 5 inclusive!"
        if end not in range(1, 6):
            message = "Endurance must be in the range of 1 to 5 inclusive!"
        if know not in range(1, 6):
            message = "Knowledge must be in the range of 1 to 5 inclusive!"
        if cunning not in range(1, 6):
            message = "Cunning must be in the range of 1 to 5 inclusive!"
        if pres not in range(1, 6):
            message = "Presence must be in the range of 1 to 5 inclusive!"
        if will not in range(1, 6):
            message = "Willpower must be in the range of 1 to 5 inclusive!"

        if message is not None:
            raise AttributeInvalidError(None, message)

        # is each core rank equal to sum of attributes?
        message = None
        if agi + dex != spd:
            message = "Speed must be the sum of agility and dexterity!"
        if stren + end != pwr:
            message = "Power must be the sum of strength and endurance!"
        if know + cunning != wits:
            message = "Wits must be the sum of knowledge and cunning!"
        if pres + will != rslv:
            message = "Resolve must be the sum of presence and willpower!"

        if message is not None:
            raise AttributeInvalidError(None, message)

        self.agi = agi
        self.dex = dex
        self.stren = stren
        self.end = end
        self.know = know
        self.cunning = cunning
        self.pres = pres
        self.will = will

        self.hp = 10 + end
        self.mp = 10 + will
        self.curhp = self.hp
        self.curmp = self.mp

        self._assign_stat_lists()

    def __init__(self, spd=6, pwr=6, wits=6, rslv=6,
                 agi=3, dex=3, stren=3, end=3, know=3, cunning=3, pres=3,
                 will=3):
        self._set_stats(spd=spd, pwr=pwr, wits=wits, rslv=rslv,
                        agi=agi, dex=dex, stren=stren, end=end,
                        know=know, cunning=cunning, pres=pres, will=will)

    def __init__(self, **kws):
        self._set_stats(spd=kws["spd"], pwr=kws["pwr"],
                        wits=kws["wits"], rslv=kws["rslv"],
                        agi=kws["agi"], dex=kws["dex"],
                        stren=kws["stren"], end=kws["end"],
                        know=kws["know"], cunning=kws["cunning"],
                        pres=kws["pres"], will=kws["will"])

class Character:
    def __init__(self, stats):
        self.stats = stats

def roll_generic(stat, threshold):
    if threshold not in (4, 6, 8):
        raise Error("Invalid roll threshold!")

    hits = 0
    botches = 0

    # roll skill or stat dice
    for i in range(0, stat):
        r = random.randrange(1, 11)
        if r == 1:
            botches += 1
        elif r >= threshold:
            hits += 1

    # roll fate dice
    for j in range(0, 5 - stat):
        r = random.randrange(1, 7)
        if r == 1:
            botches += 1
        elif r == 6:
            hits += 1

    return (stat, threshold, hits - botches, hits, botches)

if __name__ == "__main__":
    trivial_threat = Stats(**{
        "spd": 4, "pwr": 4, "wits": 4, "rslv": 4,
        "agi": 2, "dex": 2,
        "stren": 2, "end": 2,
        "know": 2, "cunning": 2,
        "pres": 2, "will": 2
    })
    challenging_threat = Stats(**{
        "spd": 6, "pwr": 6, "wits": 6, "rslv": 6,
        "agi": 3, "dex": 3,
        "stren": 3, "end": 3,
        "know": 3, "cunning": 3,
        "pres": 3, "will": 3
    })
    tough_threat = Stats(**{
        "spd": 8, "pwr": 8, "wits": 8, "rslv": 8,
        "agi": 4, "dex": 4,
        "stren": 4, "end": 4,
        "know": 4, "cunning": 4,
        "pres": 4, "will": 4
    })

    tough_npc = Character(tough_threat)

    # TODO: side-by-side bar graphs, CDF, move Stats and Char stuff
    # to different file?

    # monte carlo simulations? kinda?
    conn = sqlite3.connect(":memory:")
    c = conn.cursor()
    c.execute('''CREATE TABLE trials
(stat integer, threshold integer, outcome integer, hits integer, botches integer)''')
    for threshold in (4, 6, 8):
        for stat in range(1, 6):
            for i in range(100000):
                res = roll_generic(stat, threshold)
                c.execute("INSERT INTO trials VALUES (?,?,?,?,?)", res)

    conn.commit()

    for threshold in (4, 6, 8):
        # each list in this list represents the probs of a particular
        # outcome while the skill rank varies
        probabilities = [[] for i in range(11)]
        for stat in range(1, 6):
            outcomes = []
            for row in c.execute("SELECT outcome FROM trials WHERE threshold=? AND stat=?", (threshold, stat)):
                outcomes.append(row[0])

            for v in range(-5, 6):
                p = len([o for o in outcomes if o == v])
                p = p / len(outcomes)

                probabilities[v+5].append(np.array(p))

                # debug
                if v == 0:
                    print(np.array(p))

        # generate graph for each threshold
        labels = ["1", "2", "3", "4", "5"]
        width = 0.3
        fig, ax = plt.subplots()

        # stacked bar graph
        bot = np.full_like(probabilities[0], 0.0)
        for i in range(11):
            if i == 0:
                ax.bar(labels, probabilities[i], width,
                    label="Outcome {}".format(i-5))
            else:
                ax.bar(labels, probabilities[i], width,
                    label="Outcome {}".format(i-5),
                    bottom=bot)

            # move the bottom up so that the next bar starts
            # at the top of the previous
            bot = bot + probabilities[i]

        ax.set_xlabel("Skill rank")
        ax.set_ylabel("Probability")
        ax.set_title("Outcome probability for fixed threshold of {}".format(threshold))
        ax.legend(bbox_to_anchor=(1,1), loc="upper left")
        plt.savefig("./threshold{}".format(threshold))
        plt.clf()
        plt.cla()
        plt.close()

    conn.close()
